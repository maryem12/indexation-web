import unittest
import crowler

class TestCrawler(unittest.TestCase):
    def test_crawl_web(self):
        # Test crawling a website
        start_url = "https://www.ensai.fr/"
        max_urls = 3
        crawled_urls =crowler.crawl_web(start_url, max_urls)
        self.assertEqual(len(crawled_urls), max_urls)

        # Test crawling a website with a disallowed URL
        start_url = "https://www.facebook.com/"
        max_urls = 10
        crawled_urls =crowler.crawl_web(start_url, max_urls)
        self.assertEqual(crawled_urls, [])
    def test_extract_urls_from_sitemap(self):
        #tester sitemap
        sitemap_url = "https://ensai.fr/"
        urls =crowler.extract_urls_from_sitemap(sitemap_url)
        print(urls)
        self.assertGreater(len(urls),0)

