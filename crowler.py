import requests
from bs4 import BeautifulSoup
import time
import urllib
from urllib.robotparser import RobotFileParser
from urllib.parse import urlparse
from urllib.request import urlopen

def crawl_web(start_url, max_urls):
    # Initialize variables
    urls_to_crawl = [start_url]
    crawled_urls = []

    # Check robots.txt
    if not check_robots_txt(start_url):
        return []

   
    with open("crawled_webpages.txt", "w") as f:
        while len(crawled_urls) < max_urls and len(urls_to_crawl) > 0:
            # Get next URL to crawl
            url = urls_to_crawl.pop(0)

            # Skip URL if already crawled
            if url in crawled_urls:
                continue
            if check_robots_txt(url):
                try:
                    page = requests.get(url)
                    soup = BeautifulSoup(page.content, "html.parser")
                except:
                    continue

                # Extract links
                links = extract_links(soup)
                urls_to_crawl.extend(links)
            
            # Add URL to crawled list
                crawled_urls.append(url)

                # Write URL to file
                f.write(url + "\n")

                # Wait for 5 seconds
                time.sleep(5)
            else:
                continue
            
    return crawled_urls

def check_robots_txt(url):
    # Initialize robot parser
    rp =RobotFileParser()
    rp.set_url(urlparse(url).scheme + "://"+ urlparse(url).hostname +"/robots.txt")
    rp.read()
    # Check if URL is allowed to be crawled
    allowed = rp.can_fetch("*", url)
    if allowed:
        return True
    else:
        return False

 
      
def extract_links(soup):
    links = [link.get("href") for link in soup.find_all("a")]
    return links
def extract_urls_from_sitemap(url):
    rp=urllib.robotparser.RobotFileParser()
    rp.set_url(urlparse(url).scheme + "://"+ urlparse(url).hostname +"/robots.txt")
    try: 
        rp.read()
    except :
        return []  
    sitemaps=rp.site_maps() 
    try:
        url_from_site_map=[]
        for sitemap in sitemaps:
            response=urlopen(sitemap)
            soup=BeautifulSoup(response,'lxml',from_encoding=response.info().get_param('charset'))
            urls=soup.find_all("url")
            for url in urls:
                loc=url.findNext("loc").text
                url_from_site_map.append(loc)
        return url_from_site_map
    except:
        return[]

