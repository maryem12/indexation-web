## Quick start

Pour lancer rapidement le code veuillez créer deux terminaux :

-exécutez ce code:

#### installer les dépendances
```
pip install -r requirements.txt

```
#### lancer le crowler
```
python3 main.py --url https://ensai.fr/
```
#### Pour tester:
```
python -m unittest Test.py
```